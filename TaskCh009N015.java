/**9.15. Дано слово. Вывести на экран его k-й символ. **/
 
import java.util.Scanner;

public class word {
    public static void main(String[] args) {
        Scanner SCANNER = new Scanner(System.in);
        System.out.println("Enter Word: ");
        String WORD  = SCANNER.nextLine();
        System.out.println("k-symbol of word. Enter number: ");
        int k = SCANNER.nextInt();
        System.out.println(WORD.charAt(k+1));
    }
}