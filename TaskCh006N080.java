
/**6.80. Дано  натуральное  число.  Определить,  какая  цифра  встречается  в  нем  чаще:
 0 или 9.

 Автор: Шапошников Сергей **/

import java.util.Scanner;
 public class S {
    public static void main(String[] args) {
        int i;
        int j = 0;
        int k = 0;

        Scanner SCANNER = new Scanner(System.in);
        int a = SCANNER.nextInt();
        String value = Integer.toString(a);
        int l = value.length();
        for (i = 0; i < l; i++) {
            char sss = value.charAt(i);
            System.out.println(sss);
            if (sss == '0') j++;
            if (sss == '9') k++;
        }
        System.out.println("Количество 0: " + j + "\n" + "Сколько  9: " + k);
    }
}
