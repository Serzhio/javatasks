/**9.42. Составить программу, которая печатает заданное слово, начиная с последней
 буквы.
 use StringBuilder**/

import java.util.Scanner;

public class word {
    public static void main(String[] args) {
        Scanner SCANNER = new Scanner(System.in);
        System.out.println("Enter Word: ");
        StringBuilder WORD  = new StringBuilder(SCANNER.nextLine());
        System.out.println(WORD.reverse());
    }
}