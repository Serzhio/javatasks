/**
10.53. Написать рекурсивную процедуру для ввода с клавиатуры последовательно-
сти чисел и вывода ее на экран в обратном порядке (окончание последова-
тельности — при вводе нуля). 
**//

import java.util.*;

public class TaskCh010N053 {

    public static String reverseString(String k) {
        int i = k.length();
        StringBuilder result = new StringBuilder();
        for (int j = 0; j < i; j++) {
            result.insert(0, k.charAt(j));
        }
        return result.toString();
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter array length: ");
        int size = input.nextInt();
        int array[] = new int[size];
        System.out.println("Insert array elements:");
        for (int i = 0; i < size; i++) {
            array[i] = input.nextInt();
            System.out.print("Inserted array elements:");
        }
        System.out.print (Arrays.toString(array) + "\n");
        System.out.print(reverseString(Arrays.toString(array)));
    }
}
