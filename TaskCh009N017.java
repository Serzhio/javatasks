/**9.17. Дано  слово.  Верно  ли,  что  оно  начинается  и  оканчивается  на  одну  и  ту  же
 букву?**/

import java.util.Scanner;

public class word {
    public static void main(String[] args) {
        Scanner SCANNER = new Scanner(System.in);
        System.out.println("Enter Word: ");
        String WORD  = SCANNER.nextLine();
        int l = WORD.length();
        if (WORD.charAt(0) == WORD.charAt(l-1)) System.out.println("YES!");
        else System.out.println("NO");
    }
}