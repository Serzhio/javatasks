import java.util.*;

import static java.lang.System.out;

public class TaskCh012N023 {
    public static void main(String[] args) {


        Scanner SCANNER = new Scanner(System.in);
        out.println("Введите размерность матрицы N: ");
        int n = SCANNER.nextInt();
        int[][] a = new int[n][n];
        /** a) **/
/*
        if ((n % 2) != 0) {
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++) {
                    if ((i == j) | (i == n - j - 1)) {
                        a[i][j] = 1;
                    }
                }
        } else {
            out.println("Error");*/

        /** б) **/
         /*
        if ((n % 2) != 0) {
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++) {
                    if ((i == j) | (i == n - j - 1)|(i == (n-1)/2)|(j == (n-1)/2)) {
                        a[i][j] = 1;
                    }
                }
        } else {
            out.println("Error");
        }*/
        /** в) **/

        if ((n % 2) != 0) {
            for (int i = 0; i < (((n - 1) / 2) +1); i++)
                for (int j = i; j < n - i; j++) {
                    a[i][j] = 1;
            }
            for (int k = (((n/ 2) + 1)); k < n; k++) {
                for (int l = n - k - 1; l < k + 1; l++) {
                    a[k][l] = 1;
                }
            }
        }else {
            out.println("Error");
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }
}
