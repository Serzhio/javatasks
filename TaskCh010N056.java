/**
 * 10.56.* Написать рекурсивную функцию, определяющую, является ли заданное на-
 * туральное  число  простым  (простым  называется  натуральное  число,  боль-
 * шее 1, не имеющее других делителей, кроме единицы и самого себя).
 */

import java.util.*;

public class TaskCh010N055 {
    public static boolean CheckDigit(int n, int i) {
        if (n < 2) {
            return false;
        } else {
            if (n == 2) {
                return true;
            } else {
                if (n % i == 0) {
                    return false;
                } else if (i * i <= n) {
                    return CheckDigit(n, i + 1);
                } else return true;
            }
        }
    }

    public static void main(String[] args) {
        System.out.println("Введите число: ");
        Scanner SCANNER = new Scanner(System.in);
        int n = SCANNER.nextInt();
        System.out.println(CheckDigit(n,2));
    }
}
