import static java.lang.System.out;

/**12.63. В  двумерном  массиве  хранится  информация  о  количестве  учеников  в  том
 или ином классе каждой параллели школы с первой по одиннадцатую (в пер-
 вой строке — информация о количестве учеников в первых классах, во вто-
 рой — о вторых и т. д.). В каждой параллели имеются 4 класса. Определить
 среднее количество учеников в классах каждой параллели.
 **/


public class TaskCh012N063 {
    public static void main(String[] args) {
        int m = 11;
        int n = 4;
        int[][] matrix = new int[m][n];
        int [] avg = new int [m];
        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++) {
                matrix[i][j] = (1 + (int) (Math.random() * 25));
            }
       /*    //Вывод матрицы всех учеников по классам.
              out.println("Result matrix:" + "\n");
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                //out.print("Class № " + i + " " +matrix[i][j] + " ");
                out.print(matrix[i][j] + " ");
            }
            out.println();

        }
        out.println("\n");*/

        for (int i = 0; i < m; i++){
            int sum = 0;
            for (int j = 0; j < n; j++) {
                sum += matrix[i][j];

            }
            out.println("В среднем, на всех параллелях в классе №" + (i+1) + " " + Math.round(sum/4) + " учеников" + " ");
        }



    }
}
