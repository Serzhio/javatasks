/*10.52. Написать рекурсивную процедуру для вывода на экран цифр натурального 
числа в обратном порядке.*/

import java.util.Scanner;

    class recursion {
        int recursio(int n) {
            if (n < 10) {
                return n;
            } else {
                System.out.print(n % 10);
                return recursio(n / 10);
            }
        }
    }

        public class TaskCh010N052 {
            public static void main(String[] args) {
                Scanner Scan = new Scanner(System.in);
                int k = Scan.nextInt();
                recursion f = new recursion();
                System.out.println(f.recursio(k));
            }
        }
