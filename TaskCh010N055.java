/**
 *
 * 10.55.* Написать рекурсивную процедуру перевода натурального числа из десятич-
 * ной системы счисления в N-ричную. Значение N в основной программе вво-
 * дится с клавиатуры (2   N   16).
 */

import java.io.*;
import java.util.Scanner;
public class TaskCh010N054 {

    public static String getDigit(int number, int N){
        String[] digits = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};
        return (number == 0) ? "" : getDigit(number / N,N) + digits[number % N];
    }

    public static void main(String[] args) throws IOException{
        System.out.println("Введите число: ");
        Scanner SCANNER = new Scanner(System.in);
        int n = SCANNER.nextInt();
        System.out.println("Введите систему счисления: ");
        int i = SCANNER.nextInt();
        System.out.println(getDigit(n,i));
    }
}
