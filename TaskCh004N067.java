/** 4.67. Дано целое число k (1   k   365). Определить, каким будет k-й день года: вы-
 ходным (суббота и воскресенье) или рабочим, если 1 января — понедельник.

 Автор: Шапошников Сергей
 **/

import java.util.Scanner;

public class weekday {
    public static void main(String[] args) {
        int mink = 1;
        int maxk = 365;
        int k;
        Scanner SCANNER = new Scanner(System.in);
        System.out.println("Enter date (1 <= k <= 365)");
        k = SCANNER.nextInt();

        if ((k > maxk)|(k < mink)){
            System.out.println("That is not a valid entry. Enter date: 1 <= t <= 365: ");
            k = SCANNER.nextInt();
        }
        int t = k%7;

         /** Добавил когда посмотрел задание, сперва реализовал раскоменченный вариант **//*
        if((t >= 1)&(t <= 5)){
            System.out.println("Workday!");
        }else {
            System.out.println("Weekend!!");
        }*/
        /** но сперва реализовал так: **/
        switch (t) {
                case 1:
                    System.out.println("Monday");
                    break;
                case 2:
                    System.out.println("Tuesday");
                    break;
                case 3:
                    System.out.println("Wednesday");
                    break;
                case 4:
                    System.out.println("Thursday");
                    break;
                case 5:
                    System.out.println("Friday");
                    break;
                case 6:
                    System.out.println("Saturday");
                    break;
                case 0:
                    System.out.println("Sunday");
                    break;



            }

        }
    }