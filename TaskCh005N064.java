/** 5.64. В  области  12  районов.  Известны  количество  жителей  (в  тысячах  человек)
 и площадь (в км2) каждого района. Определить среднюю плотность населения
 по области в целом.

 Автор: Шапошников Сергей **/

import java.util.Scanner;

public class average {
    public static void main(String[] args) {
        int i;
        int Reg = 12;
        int density = 0;
        Scanner SCANNER = new Scanner(System.in);

        for (i=1;i<=Reg;i++){
            System.out.println("Введите плотность населения в " + i+"-ом регионе");
            density = density + SCANNER.nextInt();
            //System.out.println(density);
        }
        int Average = density/12;
        System.out.println("Плотность населения: " + Average);
    }
}
