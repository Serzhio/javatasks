/**10.42. В  некоторых  языках  программирования  (например,  в  Паскале)  не  преду-
 смотрена  операция  возведения  в  степень.  Написать  рекурсивную  функцию
 для расчета степени n вещественного числа a (n — натуральное число). **/

import java.util.Scanner;

class Power {
    double P(double a, int n) {
        double result;

        if (n == 1)
            return a;
        result = a*P(a, n - 1);
        return result;
    }
}

public class word {
    public static void main(String[] args) {
        Scanner SCANNER = new Scanner(System.in);
        Power f = new Power();
        System.out.println("Введите число a: ");
            double usernumber =  SCANNER.nextDouble();
        System.out.println("Введите степень числа: ");
            int step = SCANNER.nextInt();
            System.out.println("Число n " + usernumber + " в степени " + step + " "
                    + f.P(usernumber,step));
    }
}


