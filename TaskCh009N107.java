/**9.107. Дано  слово.  Поменять  местами  первую  из  букв  а  и  последнюю  из  букв о.
 Учесть возможность того, что таких букв в слове может не быть**/

import java.util.Scanner;

public class word {
    public static void main(String[] args) {

        int i,j;

        Scanner SCANNER = new Scanner(System.in);
        StringBuilder s = new StringBuilder(SCANNER.nextLine());
        System.out.println("Enter your word:");
        System.out.println(s);
        i = s.toString().indexOf('a');
        j = s.toString().lastIndexOf('o');
        if ((i == -1)|(j == -1 )) {
            System.out.println("Enter word which includes a and o");
            return;
        }

        s.setCharAt(i,'o');
        s.setCharAt(j,'a');
        System.out.println(s);
        }
    }


