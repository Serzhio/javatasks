/**10.46. Даны первый член и знаменатель геометрической прогрессии. Написать ре-
 курсивную функцию: 
 а) нахождения n-го члена прогрессии; 
 б) нахождения суммы n первых членов прогрессии. **/

import java.util.Scanner;

class Elements {
    double Elem (double a1, double d, int n){
        if (n == 1) return a1;
        else return d*Elem(a1, d,n-1);
    }
}

class progression {
    double Summaa (double a1, double d, int n){
        Elements f = new Elements();
        if (n == 1) return a1;
        else return f.Elem(a1,d,n) + Summaa(a1,d,n - 1);
    }
}

public class Element {
    public static void main(String[] args) {
        Scanner SCANNER = new Scanner(System.in);
        System.out.println("Введите первый элемент a1:");
        double a1 = SCANNER.nextDouble();
        System.out.println("Введите знаменатель d:");
        double d = SCANNER.nextDouble();
        System.out.println("Введите порядковый номер члена прогрессии:");
        int n = SCANNER.nextInt();
        Elements f = new Elements();
        progression g = new progression();
        System.out.println("а) n-ный член прогрессии;");
        System.out.println(f.Elem(a1, d, n));
        System.out.println("б) сумма n первых членов прогрессии.  ");
        System.out.println(g.Summaa(a1, d, n));

    }
}