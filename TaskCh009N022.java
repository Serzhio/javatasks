/**9.22. Дано слово, состоящее из четного числа букв. Вывести на экран его первую
 половину, не используя оператор цикла.**/

import java.util.Scanner;

public class word {
    public static void main(String[] args) {
        Scanner SCANNER = new Scanner(System.in);
        System.out.println("Enter Word: ");
        String WORD  = SCANNER.nextLine();
        int l = WORD.length();
        if (l%2 == 0) System.out.println(WORD.substring(0,l/2));
        else System.out.println("Enter a word with an even number of letters ");
    }
}