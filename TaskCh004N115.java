
/**4.115.*В некоторых странах Дальнего Востока (Китае, Японии и др.) использовался
 (и неофициально используется в настоящее время) календарь, отличающийся
 от применяемого нами. Этот календарь представляет собой 60-летнюю цик-
 лическую систему. Каждый 60-летний цикл состоит из пяти 12-летних под-
 циклов. В каждом подцикле года носят названия животных: Крыса, Корова,
 Тигр, Заяц, Дракон, Змея, Лошадь, Овца, Обезьяна, Петух, Собака и Свинья.
 Кроме того, в названии года фигурируют цвета  животных, которые связаны
 с пятью элементами природы — Деревом (зеленый), Огнем (красный), Зем-
 лей (желтый), Металлом (белый) и Водой (черный). В результате каждое жи-
 вотное (и его год) имеет символический цвет, причем цвет этот часто совер
 шенно не совпадает с его "естественной" окраской — Тигр может быть чер-
 ным,  Свинья —  красной,  а Лошадь —  зеленой.  Например,  1984 год —  год
 начала очередного цикла — назывался годом Зеленой Крысы. Каждый цвет
 в  цикле  (начиная  с  зеленого)  "действует"  два  года,  поэтому  через  каждые
 60 лет имя года (животное и его цвет) повторяется.
 Составить программу, которая по заданному номеру года нашей эры n печа-
 тает его название по описанному календарю в виде: "Крыса, Зеленый". Рас-
 смотреть два случая:
 а) значение n   1984;
 б) значение n может быть любым натуральным числом.

 Автор: Шапошников Сергей
**/

import java.lang.*;
import java.util.Scanner;

 public class chinayears {
     public static void main(String[] args) {
         int YEAR, CYEAR,NYEAR,Yearindex,Colorindex;

         String [] animals = {"Крыса","Корова","Тигр","Заяц","Дракон","Змея","Лошадь","Овца","Обезьяна","Петух","Собака","Свинья"};
         String [] colors = {"Зеленный","Красный","Желтый","Белый","Черный"};
         Scanner SCANNER = new Scanner(System.in);;
         System.out.println("Введите год, который вас интересует:");
         YEAR = SCANNER.nextInt();
         /* а) значение n   1984; 
         if (YEAR < 1984) {
             System.out.println("invalid year! please enter year >= 1984! :");
             return;
         }*/
         System.out.println(YEAR);
         CYEAR = (YEAR + 2397)%60;
         // переведем наш календарь в китайский
         if (CYEAR != 0) {
             NYEAR = CYEAR;
             System.out.println(NYEAR);
         }else{
             NYEAR = 60;
             System.out.println(NYEAR);
         }

         int AnimalYear = NYEAR%12;
         int AnimalColor = NYEAR%10;

         if (AnimalYear != 0) {
            Yearindex = AnimalYear - 1;
         }else{
             Yearindex = 11;
         }

         if (AnimalColor != 0){
             Colorindex = ((AnimalColor + 1)/2) - 1;
         }else {
             Colorindex = 4;
         }

         System.out.println(YEAR + " " +colors[Colorindex] + " " + animals[Yearindex]);
     }
}
