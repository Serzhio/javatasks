/**
 12.24. Заполнить массив размером 6x6 так, как показано на рис. 12.2.
 **/
import java.util.Arrays;

public class TaskCh012N024 {
    public static void main(String[] args) {
        int n = 6;
        int[][] matrix = new int[n][n];

        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++) {
                if ((i == 0) || (j == 0)) {
                    matrix[i][j] = 1;
                } else {
                    matrix[i][j] = matrix[i - 1][j] + matrix[i][j - 1];
                }
            }
        System.out.println("a)");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("\n" + Arrays.deepToString(matrix));

        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++) {
               matrix[i][j] = (i + j)%n + 1;
            }
        System.out.println("\n" + "a)" + "\n");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

}
