/**10.44. Написать рекурсивную функцию нахождения цифрового корня натурального
 числа. Цифровой корень данного числа получается следующим образом. Ес-
 ли  сложить  все  цифры  этого  числа,  затем  все  цифры  найденной  суммы
 и повторять этот процесс, то в результате будет получено однозначное число
 (цифра), которая и называется цифровым корнем данного числа.**/

import java.util.Scanner;

class SSquare {
    int recursion(int n) {
        int result;
        if (n == 0) {
            return n;
        } else {
            result = recursion(n / 10) + (n % 10);
            if (result > 9){
                return recursion(result);
            }
            return result;
            }
        }
    }

public class SumSq {
    public static void main(String[] args) {
        System.out.println("Enter number n: ");
        Scanner SCANNER = new Scanner(System.in);
        int n = SCANNER.nextInt();
        SSquare f = new SSquare();
        System.out.println("Result: " + f.recursion(n));

    }
}
