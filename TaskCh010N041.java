/**10.41. Написать рекурсивную функцию для вычисления факториала натурального
 числа n.**/

import java.util.Scanner;

class Factorial {
    // рекурсивный метод
    int fact(int n) {
        int result;

        if (n == 1)
            return 1;
        result = fact(n - 1) * n;
        return result;
    }
}
public class word {
    public static void main(String[] args) {
        int i;

        System.out.println("Введите число n:");
        Scanner SCANNER = new Scanner(System.in);

            Factorial f = new Factorial();
            // получим число, введенное пользователем
            int usernumber =  SCANNER.nextInt();
            System.out.println("Факториал числа" + usernumber + " равен "
                    + f.fact(usernumber));
        }
    }