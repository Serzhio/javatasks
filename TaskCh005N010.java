/** 5.10. Напечатать таблицу перевода 1, 2, ... 20 долларов США в рубли по текущему
 курсу (значение курса вводится с клавиатуры).

 Автор: Шапошников Сергей
 **/
 import java.lang.*;
import java.util.Scanner;
public class dollarrate {
    public static void main(String[] args) {
        int i;
        Scanner SCANNER = new Scanner(System.in);
        System.out.println("Enter dollar rate:");
        //int i = Rubrate.length;
        int Rate = SCANNER.nextInt();

        for (i=1; i<=20; i++) {
            System.out.println("You will get " + (Rate*i)+ " rubles for " + i + "$" + "\n");
        }
    }
}