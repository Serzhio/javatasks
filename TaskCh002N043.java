/** 2.43. Даны два целых числа a и b. Если a делится на b или b делится на a, то вывес-
ти 1, иначе — любое другое число. Условные операторы и операторы цикла
не использовать. 
Автор: Шапошников Сергей
**/

import java.util.Scanner;
import java.lang.*;
public class comparison {
    private static final Scanner SCANNER = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Enter number a =");
        Integer a = SCANNER.nextInt();
        System.out.println("a = " + a);
        System.out.println("Enter number b =");
        Integer b = SCANNER.nextInt();
        System.out.println("b = " + b);

        int c = a % b;
        int d = b % a;
        System.out.println("Solution: " + ((c*d)+1));

    }
}