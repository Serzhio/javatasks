/**
 * 10.50. Написать рекурсивную функцию для вычисления значения так называемой 
 *функции Аккермана для неотрицательных чисел n и m. Функция Аккермана 
 *определяется следующим образом: 
 */

import java.util.*;
import java.lang.*;
class MaxElem {

    public static int recursion(int n, int m) {
        if (n == 0) {
            return m + 1;
        }
        else if (m == 0 && n > 0) {
            return recursion(n - 1, 1);
        }
        else {
            return recursion(n - 1, recursion(n, m - 1));
        }
    }

    public static void main(String[] args)
    {
        Scanner SCANNER = new Scanner(System.in);
        System.out.println("Enter n: ");
        int n = SCANNER.nextInt();
        System.out.println("Enter m: ");
        int m = SCANNER.nextInt();
        System.out.println("Accerman's Function: " + recursion(n,m));
    }
    }



