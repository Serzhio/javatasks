/**4.106. Составить программу, которая в зависимости от порядкового номера дня ме-
 сяца  (1,  2, ...,  12)  выводит на  экран  время  года,  к  которому  относится  этот
 месяц.

 Автор: Шапошников Сергей
 **/

import java.util.Scanner;
public class season {
    public static void main(String[] args) {
        String SEASON;
        Scanner SCANNER = new Scanner(System.in);
        System.out.println("Enter month:");
        int month = SCANNER.nextInt();
        switch (month){
            case 12:
            case 1:
            case 2:
                SEASON = "Winter";
                break;
            case 3:
            case 4:
            case 5:
                SEASON = "Spring";
                break;
            case 6:
            case 7:
            case 8:
                SEASON = "Summer";
                break;
            case 9:
            case 10:
            case 11:
                SEASON = "Winter is Coming";
                break;
            default:
                SEASON = "What plannet are you from? :-)";
        }
        System.out.println("Season of year = " + SEASON);
    }
}
