/** 4.15
 Известны год и номер месяца рождения человека, а также год и номер месяца
 сегодняшнего  дня  (январь —  1  и  т. д.).  Определить  возраст  человека  (число
 полных  лет).  В  случае  совпадения  указанных  номеров  месяцев считать, что
 прошел полный год.
 Автор: Шапошников Сергей
 **/
import java.util.Scanner;
import java.util.Calendar;
import java.util.concurrent.atomic.AtomicInteger;

public class Age {

    public static void main(String[] args) {
        int Year = 2017;
        int Month = 8;

        Scanner SCANNER = new Scanner(System.in);
        System.out.println("Введите год:");
        int YearB = SCANNER.nextInt();
        System.out.println("Год рождения:" + YearB);
        System.out.println("Введите месяц рождения:");
        int MB = SCANNER.nextInt();
        System.out.println("месяц рождения:" + MB);
        int TotalAge = Year - YearB;

        if (Month < MB){
            System.out.println("Возраст = " + (TotalAge - 1));
        }else{
            System.out.println("Возраст = " +  (TotalAge));
        }
    }
}