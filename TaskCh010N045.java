/**10.45. Даны первый член и разность арифметической прогрессии. Написать рекур-
 сивную функцию для нахождения:
 а) n-го члена прогрессии;
 б) суммы n первых членов прогрессии.**/

import java.util.Scanner;

class Elements {
    double Elem (double a1, double d, int n){
        if (n == 1) return a1;
        else {
            double result = Elem(a1,d,n-1) + d;
            return result;
        }
    }
}

class progression {
    int Summaa (int a1, int d, int n){
        int sum = 0;
        if (n == 1) return a1;
        else {
            sum = a1 + (n - 1) * d + Summaa(a1,d,a1);
            return sum;
        }
    }
}

public class Element {
    public static void main(String[] args) {
        Scanner SCANNER = new Scanner(System.in);
        System.out.println("Введите первый элемент a1:");
        int a1 = SCANNER.nextInt();
        System.out.println("Введите разность d:");
        int d = SCANNER.nextInt();
        System.out.println("Введите число n:");
        int n = SCANNER.nextInt();
        Elements f = new Elements();
        progression g = new progression();
        System.out.println("а) вычисления суммы цифр натурального числа; ");
        System.out.println(f.Elem(a1, d, n));
        System.out.println("б) вычисления количества цифр натурального числа. ");
        System.out.println(g.Summaa(a1, d, n)); // вызов рекурсивной функции
    }
}