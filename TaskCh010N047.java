/**10.47**/
        
import java.util.Scanner;

class Fib {
    int f(int k){
        if (k == 0){
            return 0;
        } else if (k <= 2){
            return 1;
        } else {
            return f(k-1) + f(k -2);
        }
    }
}

public class fibb {
    public static void main(String[] args) {
        Scanner SCANNER = new Scanner(System.in);
        System.out.println("Введите искомый элменет k:");
        int k = SCANNER.nextInt();
        Fib ff = new Fib();
        System.out.println(ff.f(k));
    }
}