/**
 * 12.234. Дан двумерный массив.
 * а) Удалить из него k-ю строку.
 * б) Удалить из него s-й столбец.
 */
import java.util.Scanner;
import static java.lang.System.*;

public class TaskCh012N234 {
    public static void main(String[] args) {
        int n = 10;
        Scanner SCANNER = new Scanner(in);
        out.println("Введите k, номер строки которую надо удалить: от 1 до: " + n);
        int k = SCANNER.nextInt();
        out.println("Введите s, номер столбца который надо удалить. от 1 до: " + n);
        int s = SCANNER.nextInt();
        int[][] matrix = new int[n][n];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = (int) (Math.random() * 10);
            }
        }
        out.println("The Matrix:\n");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                out.print(matrix[i][j] + " ");
            }
            out.println();
        }
        if ((k < n) & (s < n) & (k > -1) & (s > -1)) {


            for (int i = k; i < n - 1 ; i++) arraycopy(matrix[i + 1], 0, matrix[i], 0, n - 1);
            for (int i = 0; i < n - 1 ; i++) arraycopy(matrix[i], s + 1, matrix[i], s, n - 1 - s);
            for (int i = 0; i < n; i++) 
                for (int j = 0; j < n; j++) {
                    matrix[i][n-1] = 0;
                    matrix[n-1][j] = 0;
            }

            out.println("Result matrix:" + "\n");
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    out.print(matrix[i][j] + " ");
                }
                out.println();
            }

        }else out.println("Nice Try! *CRAZY*");
    }
}
