/** 2.39
Даны  целые  числа  h,  m,  s  (0 < h ≤ 23,  0 ≤ m ≤ 59,  0 ≤ s ≤ 59),  указывающие
момент времени: "h часов, m минут, s секунд". Определить угол (в градусах)
между  положением  часовой  стрелки  в  начале  суток  и  в  указанный  момент
времени.

Автор: Шапошников Сергей
**/



import java.util.Arrays;
import java.util.Scanner;
import java.lang.*;
public class Lists {
    private static final Scanner SCANNER = new Scanner(System.in);


    public static void main(String[] args) {
        int min = 0;
        int maxh = 23;
        int maxm = 59;


        System.out.println("Enter values h 0<h<=23:");
        Integer h = SCANNER.nextInt();
        do{
            if ((h > maxh)|(h <= min)){
                System.out.println("That is not a valid entry. Enter values h: 0<h<=23: ");
                h = SCANNER.nextInt();
            }
        }
        while ((h > maxh)|(h <= min));

        System.out.println("Enter values m 0<m<=59:");
        Integer m = SCANNER.nextInt();
        do{
            if ((m > maxm)|(m < min)){
                System.out.println("That is not a valid entry. Enter values 0<=m<=59: ");
                m = SCANNER.nextInt();
            }
        }while ((m > maxm)|(m < min));

        System.out.println("Enter values s 0<=s<=59:");
        Integer s = SCANNER.nextInt();
        do{
            if ((s > maxm)|(s < min)){
                System.out.println("That is not a valid entry. Enter values 0<=s<=59: ");
                s = SCANNER.nextInt();
            }
        }while ((s > maxm)|(s < min));

        String[] data = {h+":"+m +":"+s};
        System.out.println("Time = " + Arrays.toString(data));
        //Integer s = SCANNER.nextInt();

        Integer Grad = (h*3600 + m*60 + s)/120; //считаю по секундам.

        if (Grad == 0) System.out.println("Grad = 0");
        else System.out.println("Grad = " + Grad);
    }
}