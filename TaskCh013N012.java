package worker;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *13.12. Известна  информация  о  20 сотрудниках  фирмы:  фамилия,  имя,  отчество,
 * адрес  и  дата  поступления  на  работу  (месяц,  год).  Напечатать  фамилию,
 * имя, отчество и адрес сотрудников, которые на сегодняшний день прорабо-
 * тали в фирме не менее трех лет. День месяца не учитывать (при совпадении
 * месяца поступления и месяца сегодняшнего дня считать,  что прошел полный год).
 * @author USER
 */


public class TaskCh013N012 {
    public static void main(String[] args) {

        worker[] work = new worker[20];
        work[0] = new worker("Stark", "Eddard", "Starkovich", "Winterfell, Great House of Westeros", new GregorianCalendar(1997, 0, 10));
        work[1] = new worker("Snow", "Jon", "Targaryenovich", "Night's Watch", new GregorianCalendar(2015, 1, 11));
        work[2] = new worker("Stark", "Arya", "Starkovna", "Winterfell, Great House of Westeros", new GregorianCalendar(1999, 2, 12));
        work[3] = new worker("Daenerys", "Targaryen", "Targaryevna", "House Targaryen", new GregorianCalendar(2000, 3, 13));
        work[4] = new worker("Greyjoy", "Theon", "Greyjoyovich", "House Greyjoy, Lords Paramount of the Iron Islands ", new GregorianCalendar(2001, 4, 14));
        work[5] = new worker("Tyrell", "Mace", "Tyrellivna", "Highgarden", new GregorianCalendar(2002, 5, 15));
        work[6] = new worker("Baratheon", "Stannis", "Baratheonovich", "Storm’s End", new GregorianCalendar(2003, 6, 16));
        work[7] = new worker("Stark", "Sansa", "Starkovna", "Winterfell, Great House of Westeros", new GregorianCalendar(2016, 7, 17));
        work[8] = new worker("Lannister", "Jaime", "Kingslayerovich", "Casterly Rock in the Westerlands", new GregorianCalendar(2004, 8, 18));
        work[9] = new worker("Drogo", "Khal", "Drogovich", "Dothraki triba", new GregorianCalendar(2014, 9, 19));
        work[10] = new worker("Karstark", "Rickard", "Karstarkovich", "Karhold, castle #1", new GregorianCalendar(2005, 10, 20));
        work[11] = new worker("Martell", "Doran", "Martellovich", "Sunspear, Dorne", new GregorianCalendar(2015, 11, 21));
        work[12] = new worker("Martell", "Trystane ", "Martellovich", "Sunspear, Dorne", new GregorianCalendar(2006, 0, 22));
        work[13] = new worker("Night", "The", "King", "Lands of Always Winter", new GregorianCalendar(2016, 1, 23));
        work[14] = new worker("Bolton", "Ramsey", "Boltonovich", "The Dreadfort", new GregorianCalendar(2007, 2, 24));
        work[15] = new worker("Bolton", "Roose", "Boltonovich", "The Dreadfort", new GregorianCalendar(2017, 3, 25));
        work[16] = new worker("of", "Brienne", "Tarth", "Evenfall Hall", new GregorianCalendar(2008, 4, 26));
        work[17] = new worker("Stark", "Bran", "Starkovich", "ddff", new GregorianCalendar(2016, 5, 27));
        work[18] = new worker("Mormont", "Lyanna", "Mormontovna", "Bear Island, the North", new GregorianCalendar(2009, 6, 28));
        work[19] = new worker("Worm", "Grey", "unknown", "Summer Isles", new GregorianCalendar(2015, 7, 29));

        GregorianCalendar pp = new GregorianCalendar();
        int testyear = pp.get(Calendar.YEAR);
        int testmnth = pp.get(Calendar.MONTH);

        for(int i = 0; i < 20; i++){
            work[i].testexp(testyear,testmnth);
        }
        System.out.println();


    }
}


class worker {
    public String surname;
    public String name;
    public String middle;
    public String address;
    public GregorianCalendar offer_date;

    public worker() {
    }

    public worker(String surname, String name, String middle, String address, GregorianCalendar offer_date){
        this.surname = surname;
        this.name = name;
        this.middle = middle;
        this.address = address;
        this.offer_date = offer_date;
    }

    public String getSurname() {
        return surname;
    }
    public String getName() {
        return name;
    }
    public String getMiddle() {
        return middle;
    }
    public String getAddress() {
        return address;
    }
    public GregorianCalendar getOffer_date() {
         return offer_date;
    }


    public void setSurname(String surname){
        this.surname = surname;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setMiddle(String middle){
        this.middle = middle;
    }
    public void setAddress(String address){
        this.address = address;
    }
    public void setOffer_Date(GregorianCalendar offer_date){
        this.offer_date = new GregorianCalendar(2017, 9, 24);
    }

    public void testexp(int testyear, int testmnth) {
        Calendar a = this.getOffer_date();
        int year = a.get(Calendar.YEAR);
        int month = a.get(Calendar.MONTH);
        double check = ((testyear - year)*12 + (testmnth + month))/12;
        if (check >= 3.0){
            // System.out.println("Surname: " + getSurname() + " Name: " + getName() + " Middle Name: " + getMiddle() + " Address: " + getAddress() );
            System.out.println(getName() + " "  + getSurname() + " " + getMiddle() + " Address: " + getAddress() );
        }


    }

}