/**
 Известны год и номер месяца рождения человека, а также год и номер месяца
 сегодняшнего  дня  (январь —  1  и  т. д.).  Определить  возраст  человека  (число
 полных  лет).  В  случае  совпадения  указанных  номеров  месяцев считать, что
 прошел полный год.
 Автор: Шапошников Сергей
 **/
import java.util.Scanner;

public class Age {

    public static void main(String[] args) {
        int TMonth;
        int TYear;
        int BMonth;
        int BYear;

        Scanner SCANNER = new Scanner(System.in);
        System.out.println("Enter Today Month Year:");
        String TMonthYear = SCANNER.nextLine();
        int TestTMonthYear = TMonthYear.length();
        if (TestTMonthYear > 7 ) {
            System.out.println("Unsupported Format! Try again!");
            return;
        }
        if (TestTMonthYear < 7 ){
            TMonth = new Integer(TMonthYear.substring(0,1));
            TYear = new Integer(TMonthYear.substring(2,6));
            //System.out.println(TMonth + ":" + TYear);
        }else{
            TMonth = new Integer(TMonthYear.substring(0,2));
            TYear = new Integer(TMonthYear.substring(3,7));
            //System.out.println(TMonth + ":" + TYear);
        }

        System.out.println("Enter BithDate Month Year:");
        String BMonthYear = SCANNER.nextLine();

        int TestBMonthYear = BMonthYear.length();
        if (TestBMonthYear > 7 ) {
            System.out.println("Unsupported Format! Try again!");
            return;
        }
        if (TestBMonthYear < 7 ){
            BMonth = new Integer(BMonthYear.substring(0,1));
            BYear = new Integer(BMonthYear.substring(2,6));
            System.out.println(BMonth + ":" + BYear);
        }else{
            BMonth = new Integer(BMonthYear.substring(0, 2));
            BYear = new Integer(BMonthYear.substring(3, 7));

        }

        System.out.println(BMonth + ":" + BYear);
        if (TMonth < BMonth){
            System.out.println("Возраст = " + (TYear - BYear - 1));
        }else{
            System.out.println("Возраст = " +  (TYear - BYear));
        }
    }
}