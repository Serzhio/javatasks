/**9.166. Дано предложение. Поменять местами его первое и последнее слово.**/
import java.util.regex.*;
public class word {
    public static void main(String[] args) {
        String S = "Поменять местами его первое и последнее слово";
        String[] sentence = S.split("[\\!|\\.|\\?]\\s?");
        String[] sentenceResult = new String[sentence.length];

        for (int i = 0; i < sentence.length; i++) {
            sentenceResult[i] = sentence[i].trim().replaceAll("(?U)^(\\w+)(.*)(\\b\\w+)([.?!]?$)", "$3$2$1$4");
        }

        for (String s : sentenceResult) {
            System.out.println(s);
        }
    }
}