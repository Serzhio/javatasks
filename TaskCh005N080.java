/**5.80. Вычислить  приближенно  площадь  фигуры,  образованной  кривой
 y=0.5x +1^2 + 2, осью абсцисс, осью ординат и прямой  y = 2.

 Автор: Шапошников Сергей **/

 public class S {
    public static void main(String[] args) {
        int a,b;
        double h,x,f;
        double S = 0;
        a = 0;
        b = 2;
        h = 0.001;
        x  = a + h;
        do {
            f = (((x)/2 + 3) + ((x - 1)/2 + 3))/2;
            S = S + f*h;
            x = x + h;
        } while (x <= b);
        System.out.println("Площадь = " + S);
    }
}
