/**
 * 10.49. Написать  рекурсивную  функцию  для  вычисления  индекса  максимального
 элемента массива из n элементов.
 */

import java.util.*;
import java.lang.*;
class MaxElem {
    public static void main(String[] args)
    {

        System.out.println(Arrays.toString(arr));
        // Выведем индекс максимального элемента
        System.out.println("Индекс максимального элемента :" + MaxIndex(arr, 0));
    }
            public static int MaxIndex(int[] k, int start) {
                if (MaxE(k, k[start]))
                    return start + 1 ;
                return MaxIndex(k, start + 1);
            }
            public static boolean MaxE(int [] g, int j) {
                //System.out.println(g.length);
                for (int i = 0; i < g.length; i++){
                    if (g[i] > j)
                        return false;
            }
            return true;
    }


}


