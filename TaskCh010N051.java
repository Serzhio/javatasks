/* 10.51. Определить  результат  выполнения  следующих  рекурсивных  процедур  при 
n5 : 
 Задание сделать не удалось согласно требованиям ТЗ, не получилось разобраться с записью и считыванием в файл:((((
 */
public class TaskCh010N051 {
    public static void main(String[] args) {
        int n = 5;
     System.out.println(recursion(n));
     System.out.println(recursion1(n));
     System.out.println(recursion2(n));
    }

    public static String recursion(int i) {
        if (i > 0) {
            System.out.println(i);
            return recursion(i - 1);
        }
        return "null";
    }



    public static String recursion1(int i) {
        if (i > 0) {
            recursion(i - 1);
            System.out.println(i);
        }
        return "null";
    }

    public static String recursion2(int i) {
        if (i > 0) {
            System.out.println(i);
            recursion(i - 1);
            System.out.println(i);
        }
        return "null";
    }

}

