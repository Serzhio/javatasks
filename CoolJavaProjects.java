package CoolJavaProject;

import java.util.stream.IntStream;

//создаем интерфейс Manager'a. с методом salutesCandidates
interface Manager {
    void salutesCandidates();
}
//Создаем интерфейс Learner. с двумя методами - sayHi & describeJavaExperience;
interface Learner {
    void describeJavaExperience();
    void sayHi();
    void sayHi(int i);
}

// создаем класс MangerImp с преминением интерфейса Manager
class MangerImp implements Manager {
    @Override
    public void salutesCandidates() {
        System.out.println("HR: " + "Hi! introduce yourelf and describe your java experience please");
    }
}
// создаем абстрактнй класс AbstractCandidate с преминением интерфейса Learner
// тут используется Инкапсуляция объектов
abstract class AbstractCandidate implements Learner {
    private final String name;
    protected final String experience;

    public AbstractCandidate(String name, String experience) {
        this.name = name;
        this.experience = experience;
    }

    @Override
    public void sayHi() {
        System.out.println(String.format("Hi! my name is %s!", name));
    }
    //Перегрузка
    public void sayHi(int i) {
        System.out.println(String.format("Candidate %s :" + "Hi! my name is %s!",i,name));
    }

}

// Создаем класс SelfLearner который наследует и переопределяет методы абстрактного класса AbstractCandidate
class SelfLearner extends AbstractCandidate {

    public SelfLearner(String name) {
        this(name, "");
    }

    public SelfLearner(String name, String experience) {
        super(name, experience);
    }

    @Override
    public void describeJavaExperience() {
        if (experience == null || experience.isEmpty()) {
            System.out.println("I have no experience :(");
            return;
        }
        System.out.println(experience + "\n");
    }
}
class GjjLearner extends AbstractCandidate {

    public GjjLearner(String name) {
        this(name, "");
    }

    public GjjLearner(String name, String experience) {
        super(name, experience);
    }

    @Override
    public void describeJavaExperience() {
        if (experience == null || experience.isEmpty()) {
            System.out.println("I have no experience :(");
            return;
        }
        System.out.println(experience + "\n");
    }
}

public class CoolJavaProjects{
    public static void main(String[] args) {


        Learner[] SLearner = new SelfLearner[5];
        Learner[] GLearner = new GjjLearner[5];
        Manager salut = new MangerImp();

        SLearner[0] = new SelfLearner("Bran Stark", "I have been learning Java by myself, nobody examined how thorough is my knowlage and how good i my code.");
        SLearner[1] = new SelfLearner("Theon Greyjoy", "I have been learning Java by myself, nobody examined how thorough is my knowlage and how good i my code.");
        SLearner[2] = new SelfLearner("Grey Worm", "I have been learning Java by myself, nobody examined how thorough is my knowlage and how good i my code.");
        SLearner[3] = new SelfLearner("Ramsey Bolton", "I have been learning Java by myself, nobody examined how thorough is my knowlage and how good i my code.");
        SLearner[4] = new SelfLearner("Trystane Martell", "I have been learning Java by myself, nobody examined how thorough is my knowlage and how good i my code.");
        GLearner[0] = new GjjLearner("Sansa Stark", "I passed succesfully getJavaJob exams and code reviews.");
        GLearner[1] = new GjjLearner("Daenerys Targaryen", "I passed succesfully getJavaJob exams and code reviews.");
        GLearner[2] = new GjjLearner("Jaime Lannister", "I passed succesfully getJavaJob exams and code reviews.");
        GLearner[3] = new GjjLearner("Khal Drogo", "I passed succesfully getJavaJob exams and code reviews.");
        GLearner[4] = new GjjLearner("Arya Stark", "I passed succesfully getJavaJob exams and code reviews.");
        // Реализуем диалог

        IntStream.range(0, 5).forEach(i -> {
            salut.salutesCandidates();
            SLearner[i].sayHi();
            SLearner[i].describeJavaExperience();
            salut.salutesCandidates();
            GLearner[i].sayHi();
            GLearner[i].describeJavaExperience();
        });
    }
}