/**
 4.33. Дано натуральное число.
 а) Верно ли, что оно заканчивается четной цифрой?
 б) Верно ли, что оно заканчивается нечетной цифрой?
 Примечание
 **/
import java.util.Scanner;

public class Chet {

    public static void main(String[] args) {
        Scanner SCANNER = new Scanner(System.in);
        System.out.println("Введите число");
        Integer Num = SCANNER.nextInt();
        System.out.println( "Число заканчивается четной цифрой? " + ( Num % 2 == 0) );
        System.out.println( "Число заканчивается нечетной цифрой? " + ( Num % 2 == 1) );
    }
}