/**10.48. Написать  рекурсивную  функцию  для  вычисления  максимального  элемента 
массива из n элементов. **/

import java.util.*;

class MaxElem {
    public static void main(String[] args) 
    {
        //Формируем массив, ,в котором бубем искать максимальный э-нт
        int[] arr = new int[20];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 1000);
        }
        //Выводим массив
        System.out.println(Arrays.toString(arr));
        // Выведем максимальный элемент
        System.out.println(MaxE(arr, 0));
    }
            public static int MaxE(int [] g, int j) {
                //System.out.println(g.length);
                if (g.length > j) {
                    int next = MaxE(g,j + 1);
                    return (g[j] > next) ? g[j] : next;
                }
                return g[0];
            }
    }


