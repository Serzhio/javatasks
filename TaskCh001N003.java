/** 1.3 Составить программу вывода на экран числа, вводимого с клавиатуры. Выво-
 димому числу должно предшествовать сообщение "Вы ввели число". 

 use java.util.Scanner class for input
 Автор: Шапошников Сергей **/

package	io.scanner;

import java.util.Scanner;

public class ScannerExample {

    public static void main(String[] args) {
        Scanner	stdInScanner	=	new	Scanner(System.in);
        int	integerBar	=	stdInScanner.nextInt();
        System.out.printf("Вы ввели число:%d", integerBar );
    }
}