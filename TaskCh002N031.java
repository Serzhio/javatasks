/** 2.31
В трехзначном числе x зачеркнули его вторую цифру. Когда к образованному 
при этом двузначному числу справа приписали вторую цифру числа x, то по-
лучилось число n. По заданному n найти число x (значение n вводится с кла-
виатуры, 100 ≤ n ≤ 999).

Автор: Шапошников Сергей
**/
 
import java.util.Scanner;
public class Lists {
    private static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {
        int min = 100;
        int max = 999;

        System.out.println("Enter 3-sign number (100<n<999):");
        int NUMBER = SCANNER.nextInt();
        String value = Integer.toString(NUMBER);
        if((NUMBER <= min) | (NUMBER >= max) ) System.out.println("Number must be in interval 100<=n<=999! Try again");
        else System.out.println("x = " + value.charAt(0) + value.charAt(2) + value.charAt(1));

    }

}