/**
 *  11.245.*Дан массив. Переписать его элементы в другой массив такого же размера
 *  следующим  образом:  сначала  должны  идти  все  отрицательные  элементы,
 *  а  затем  все  остальные.  Использовать  только  один  проход  по  исходному
 *  массиву.
 */

public class TaskCh011N245 {
    public static void main(String[] args) {
        int k1 = 0;
        int k2 = 1;
        int i;
        int[] list = {1, -2, -3, 4, 5, -6, 7, -8};
        int n = list.length;
        int[] list1 = new int[n];

        for (i = 0; i < n; i++) {
            if (list[i] < 0) {
                System.arraycopy(list, i, list1, k1, 1);
                k1++;
            } else {
                    System.arraycopy(list, i, list1, n - k2, 1);
                    k2++;
                }
            }

        for (i = 0; i < n; i++) {
            System.out.print(list1[i] + ", " );
        }
    }
}
