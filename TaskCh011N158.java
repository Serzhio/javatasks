/**
11.158.*Удалить из массива все повторяющиеся элементы, оставив их первые вхо-
ждения, т. е. в массиве должны остаться только различные элементы.
Внимание!
В задачах 11.159—11.168 под вставкой числа n в массив после k-го элемента следу-
ет понимать:
 увеличение размера массива на 1;
 смещение всех элементов, начиная с (k + 1)-го, вправо на 1 позицию;
 присваивание (k + 1)-му элементу массива значения n.
 **/
import java.util.Arrays;
import java.util.Scanner;
public class TaskCh011N158 {
    public static void main(String[] args) {

        Scanner SCANNER = new Scanner(System.in);
        System.out.println("Введите размер массива:");
        int n = SCANNER.nextInt();
        int arr[] = new int[n];
        System.out.println("Введите элементы массива:");
        for (int arr_i = 0; arr_i < n; arr_i++) {
            arr[arr_i] = SCANNER.nextInt();
        }

        int k = 0;
        int i = 0;
        while (i < n) {
            int j = 0;
            while (j < i && arr[j] != arr[i]) j++;
            if (j < i) {
                k++;
            } else if (k > 0) {
                i = i - k;
                n = n - k;
                for (j = i; j < n; j++) {
                    arr[j] = arr[j + k];
                }
                k = 0;
                System.out.println(Arrays.toString(arr));
            }
            i++;
        }

        if (k > 0) {
            i = i - k;
            n = n - k;
            System.arraycopy(arr, i + k, arr, i, n - i);
            System.out.println(Arrays.toString(arr));
        }

        for (i = n; i < arr.length; i++) arr[i] = 0;
        System.out.println(Arrays.toString(arr));
    }
}