import java.util.Scanner;

/**
 * 6.87. Составить программу, которая ведет учет очков, набранных каждой командой
 *  при игре в баскетбол. Количество очков, полученных командами в ходе игры,
 *  может  быть  равно  1,  2  или  3.  После  любого  изменения  счет  выводить  на
 *  экран.  После  окончания  игры  выдать  итоговое  сообщение  и  указать  номер
 *  команды-победительницы.  Окончание  игры  условно  моделировать  вводом
 *  количества очков, равного нулю.
 */
class Game {
    Scanner SCANNER = new Scanner(System.in);
    int ts1 = 0;
    int ts2= 0;

    void play() {
        int Team;
        int TeamScore;

        do {
            System.out.println("Enter team to score (1 or 2 or 0 to finish game): ");
            Team = SCANNER.nextInt();
            if (Team != 0) {
                System.out.println("Enter score (1 or 2 or 3): ");
                TeamScore = SCANNER.nextInt();
                if ((TeamScore != 0) && (TeamScore <= 3)) {
                    System.out.println(score(Team, TeamScore) + "\n");
                }else {
                    System.out.println(" Put (1 or 2 or 3). Try again! ");
                }
            }
        } while (Team != 0);
    }

    String score(int team, int TeamScore) {
        if (team == 1) ts1 += TeamScore;
        else if (team == 2) ts2 += TeamScore;
        return String.format("Actual Score: %d:%d", ts1, ts2);
    }

        String result(String FirstTeam, String SecondTeam, int t1, int t2){
            if (t1 > t2) System.out.println(FirstTeam + " Winner!" + "\n");
            else if (t2 > t1) System.out.println(SecondTeam + " Winner!" + "\n");
            else if (t1 == t2) System.out.println("Draw!" + "\n");
            return null;
        }
}


    public class TaskCh006N087 {
        public static void main(String[] args) {
            Scanner SCANNER = new Scanner(System.in);
            System.out.println("Enter team #1 name: ");
            String FirstTeam = SCANNER.next();
            System.out.println("Enter team #2 name: ");
            String SecondTeam = SCANNER.next();

            Game g = new Game();
            g.play();
            g.result(FirstTeam, SecondTeam, g.ts1, g.ts2);
            System.out.println("The End!" + "\n");
        }
    }

