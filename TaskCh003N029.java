/**
 *Записать условие, которое является истинным, когда:
 * @autor Шапошников Сергей
 * @verion 1.0
 */
import java.util.Scanner;
public class TaskCh003N029 {

    /** а) каждое из чисел X и Y нечетное;**/

    static boolean a(int X, int Y){
        if((X%2!=0)&(Y%2!=0)){
            return true;
        }else return false;
    }
    /** б) только одно из чисел X и Y меньше 2;**/
    static boolean b(int X, int Y){
        if((X < 2)|(Y < 2)){
            return true;
        }else return false;
    }
    /** в) хотя бы одно из чисел X и Y равно нулю;**/
    static boolean c(int X, int Y){
        if((X == 0)||(Y == 2)){
            return true;
        }else return false;
    }
    /** г) каждое из чисел X, Y, Z отрицательное;**/
    static boolean d(int X, int Y, int Z){
        if((X < 0)&(Y < 0)&(Z < 0)){
            return true;
        }else return false;
    }
    /** д) только одно из чисел X, Y и Z кратно пяти**/
    static boolean e(int X, int Y, int Z){
        int A = ((X % 5)|((X % 5) >> 1)|((X % 5) >> 2)) & 1;
        int B = ((Y % 5)|((Y % 5) >> 1)|((Y % 5) >> 2)) & 1;
        int C = ((Z % 5)|((Z % 5) >> 1)|((Z % 5) >> 2)) & 1;

        int result = (~A & B & C) | (A & ~B & C) | (A & B & ~C);
        if (result == 1){
            return true;
        }else return false;
    }
    /** е) хотя бы одно из чисел X, Y, Z  больше 100. **/
    static boolean f(int X, int Y, int Z){
        if((X > 100)&&(Y > 100)&&(Z > 100)){
            return true;
        }else return false;
    }


    public static void main(String[] args) {
        Scanner SCANNER = new Scanner(System.in);

        System.out.println("Какое решение вывести? 1,2,3,4,5?\n"
                + "1) каждое из чисел X и Y нечетное\n"
                + "2) только одно из чисел X и Y меньше 2;\n"
                + "3) хотя бы одно из чисел X и Y равно нулю\n"
                + "4) каждое из чисел X, Y, Z отрицательное\n"
                + "5) только одно из чисел X, Y и Z кратно пяти\n"
                + "6) хотя бы одно из чисел X, Y, Z  больше 100.\n");
        int n = SCANNER.nextInt();
        switch (n){
            case 1:
                System.out.println("Введите X: ");
                int X = SCANNER.nextInt();
                System.out.println("Введите Y: ");
                int Y = SCANNER.nextInt();
                System.out.println(a(X,Y) ? "Yes" : "No");
                break;
            case 2:
                System.out.println("Введите X: ");
                int X1 = SCANNER.nextInt();
                System.out.println("Введите Y: ");
                int Y1 = SCANNER.nextInt();
                System.out.println(b(X1,Y1) ? "Yes" : "No");
                break;
            case 3:
                System.out.println("Введите X: ");
                int X2 = SCANNER.nextInt();
                System.out.println("Введите Y: ");
                int Y2 = SCANNER.nextInt();
                System.out.println(c(X2,Y2) ? "Yes" : "No");
                break;
            case 4:
                System.out.println("Введите X: ");
                int X3 = SCANNER.nextInt();
                System.out.println("Введите Y: ");
                int Y3 = SCANNER.nextInt();
                System.out.println("Введите Z: ");
                int Z3 = SCANNER.nextInt();
                System.out.println(d(X3,Y3,Z3) ? "Yes" : "No");
                break;
            case 5:
                System.out.println("Введите X: ");
                int X4 = SCANNER.nextInt();
                System.out.println("Введите Y: ");
                int Y4 = SCANNER.nextInt();
                System.out.println("Введите Z: ");
                int Z = SCANNER.nextInt();
                System.out.println(e(X4,Y4,Z) ? "Yes" : "No");
                break;
            case 6:
                System.out.println("Введите X: ");
                int X5 = SCANNER.nextInt();
                System.out.println("Введите Y: ");
                int Y5 = SCANNER.nextInt();
                System.out.println("Введите Z: ");
                int Z5 = SCANNER.nextInt();
                System.out.println(f(X5,Y5,Z5) ? "Yes" : "No");
                break;
        }
    }
}
