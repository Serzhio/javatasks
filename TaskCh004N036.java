/** 4.36. Работа светофора для пешеходов запрограммирована следующим образом: в
начале каждого часа в течение трех минут горит зеленый сигнал, затем в те-
чение двух минут — красный, в течение трех минут — опять зеленый и т. д.
Дано вещественное число t, означающее время в минутах, прошедшее с нача-
ла очередного часа. Определить, сигнал какого цвета горит для пешеходов в
этот момент.

Автор: Шапошников Сергей
 **/

import  java.util.Scanner;

public class trafficlight {
    public static void main(String[] args) {
        System.out.println("Введите время в минутах");
        Scanner SCANNER = new Scanner(System.in);
        int t = SCANNER.nextInt();

        if (((t%10>=0)&(t%10<3))|((t%10>=5)&(t%10<8))){
            System.out.println("Green");
        }else{
            System.out.println("Red");
        }
    }
}
